#include<stdio.h>
#include<iostream>
#include<complex>
#include<cuComplex.h>
#include<math.h>
#include<time.h>
#include<cuda_profiler_api.h>
#include<cuda_runtime.h>
#include<cusolverDn.h>
#include<cublas_v2.h>
#include"calloc.hu"


#define CHECK(call) \
{ \
const cudaError_t error = call; \
if (error != cudaSuccess) \
{ \
printf("Error: %s:%d, ", __FILE__, __LINE__); \
printf("code:%d, reason: %s\n", error, cudaGetErrorString(error)); \
exit(1); \
} \
}
#define CHECKBLAS(call) \
{ \
const cublasStatus_t error = call; \
if (error != CUBLAS_STATUS_SUCCESS) \
{ \
printf("Error: %s:%d, ", __FILE__, __LINE__); \
printf("code:%d\n", error); \
exit(1); \
} \
}
#define CHECKSOV(call) \
{ \
const cusolverStatus_t error = call; \
if (error != CUSOLVER_STATUS_SUCCESS) \
{ \
printf("Error: %s:%d, ", __FILE__, __LINE__); \
printf("code:%d\n", error); \
exit(1); \
} \
}

#define THX 64
typedef double     rtype;
typedef cuDoubleComplex ctype;
#define rpart(x)   (cuCreal(x))
#define ipart(x)   (cuCimag(x))
#define cmplx(x,y) (make_cuDoubleComplex(x,y))

__host__ __device__ rtype carg(const ctype& z) {return (rtype)atan2(ipart(z), rpart(z));} // polar angle
__host__ __device__ rtype cabs(const ctype& z) {return (rtype)cuCabs(z);}
__host__ __device__ ctype cp2c(const rtype d, const rtype a) {return cmplx(d*cos(a), d*sin(a));}
__host__ __device__ ctype cpow(const ctype& z, const int &n) {return cmplx((pow(cabs(z), n)*cos(n*carg(z))), (pow(cabs(z), n)*sin(n*carg(z))));}
__host__ __device__ ctype cdmu(const ctype& z, const rtype a) {return cmplx( rpart(z)*a, ipart(z)*a);}
__host__ __device__ ctype cdad(const ctype& z, const rtype a) {return cmplx( rpart(z)+a, ipart(z));}


std::complex<double> make_cpp_complex( double r, double i){
    std::complex<double> a;
    a = { r, i};
    return a;
}
std::complex<double> cp_cpp_complex( std::complex<double> a){
    return a;
}
//                                          dt
//  |----------------------------------------|
//           bd 1 2 3 ......................th
//
//
//
__global__ void calc_yt_dyt( cuDoubleComplex *y, cuDoubleComplex *dy, cuDoubleComplex *a, int *delay, cuDoubleComplex *x){

    int t = blockIdx.x;
    int th = threadIdx.x;

    cuDoubleComplex ys = cmplx(0,0);
    cuDoubleComplex dys = cmplx(0,0);
    cuDoubleComplex ath = cpow(a[t],blockDim.x);

    int decay = log(1.e-13)/log(cabs(a[t]));
    int i;
    for( i=max(delay[t]-decay+th,th); i<delay[t]; i+=blockDim.x){
        ys = cuCadd( cuCmul( ys, ath), cuConj(x[i]));
        dys = cuCadd( cuCmul( dys, ath), cdmu(cuConj(x[i]),delay[t]-i-1));
    }

    extern __shared__ cuDoubleComplex y_shared[];
    y_shared[th] = ys;
    y_shared[blockDim.x+th] = dys;

    __syncthreads();
    if( threadIdx.x == 0){
        cuDoubleComplex y_k = cmplx( 0,0);
        for(int j=0; j<blockDim.x; j++){
            y_k = cuCadd( cuCmul(y_k, a[t]), y_shared[(j+delay[t]-i)%blockDim.x]);
        }
        y[t] = y_k;
    }
    if( threadIdx.x == 1){
        cuDoubleComplex dy_k = cmplx( 0,0);
        for(int j=0; j<blockDim.x; j++){
            dy_k = cuCadd( cuCmul(dy_k, a[t]), y_shared[blockDim.x+(j+delay[t]-i+1)%blockDim.x]);
        }
        dy[t] = cuCdiv( dy_k, a[t]);
    }
}
// yij = f( ai , aj* )
__global__ void calculate_yij_cuda(int Nbank, cuDoubleComplex *a1, int *delay, int length, cuDoubleComplex *yij){

    int id = threadIdx.x + blockIdx.x*blockDim.x;

    int i = id%Nbank;
    int j = id/Nbank;

    if( j < Nbank){

        cuDoubleComplex   a =         a1[i] ;
        cuDoubleComplex c_a =  cuConj(a1[j]);
        cuDoubleComplex a_d = a;
        if(i>j) a_d = c_a;

        yij[id] = cuCdiv( cmplx(1,0) , cuCsub( cmplx(1,0) , cuCmul( a , c_a)));
        yij[id] = cuCmul( yij[id] , cpow( a_d, abs(delay[i]-delay[j])));
    }
}
// dyij = dy[i][j] / da[i]
__global__ void calculate_dyij_dai( int Nbank, cuDoubleComplex *a1, int *delay, cuDoubleComplex *out){

    int id = threadIdx.x + blockDim.x * blockIdx.x;
    int i = id%Nbank;
    int j = id/Nbank;

    if( id < Nbank*Nbank){

        int k = abs( delay[i]-delay[j]);
        cuDoubleComplex   a =        a1[i] ;
        cuDoubleComplex c_a = cuConj(a1[j]);
        cuDoubleComplex a_d; 
        cuDoubleComplex tmp = cuCsub( cmplx(1,0),cuCmul(c_a,a));
        if( i==j){
            tmp = cuCdiv( cdmu(c_a,1) , cuCmul(tmp,tmp) );
        }else if( i>j){
            a_d = c_a;
            tmp = cuCdiv( cpow(a_d,k+1), cuCmul(tmp,tmp));
        }else if( i<j){
            a_d =   a;
            tmp = cuCadd( cuCdiv(cmplx(k,0),tmp) , cuCdiv( cuCmul(c_a,a), cuCmul(tmp,tmp)) );
            tmp = cuCmul( tmp , cpow(a_d,k-1) );
        }

        out[id] = tmp;
    }
}
__global__ void calculate_gradient( int Nbank, cuDoubleComplex *dyijb, cuDoubleComplex *dyt, cuDoubleComplex *b, double fact, cuDoubleComplex *grad){

    int i = threadIdx.x + blockIdx.x * blockDim.x;

    if( i >= Nbank) return;
    grad[i] = cdmu( cuCmul( cuConj(cuCsub( dyt[i], dyijb[i])), b[i]), fact);

}
__global__ void update_a1_c( int Nbank, cuDoubleComplex *a, cuDoubleComplex *G, cuDoubleComplex *mt, double *vt, double m, double beta1, double beta2, double eps){

    unsigned int i = threadIdx.x + blockIdx.x * blockDim.x;
    if( i >= Nbank) return;
    mt[i] = cuCadd( cdmu(mt[i],beta1) , cdmu( G[i], 1-beta1));
    vt[i] = vt[i]*beta2 + cuCmul(G[i],cuConj(G[i])).x*( 1-beta2);


    cuDoubleComplex hmt = cdmu( mt[i], 1./(1.-beta1));
    double hvt = vt[i]/(1-beta2);
    cuDoubleComplex da = cdmu( hmt, m/(sqrt(hvt)+eps));

    cuDoubleComplex at = cuCadd( a[i] , da);
    while( cabs(at) >= 0.9999) {
        da = cdmu( da, 1./2);
      //printf("\na[%d] = % -le % -le",i,carg(at),cabs(at));
        at = cuCadd( a[i], da);
    }
    a[i] = at;
}

void update_a1( int Nbank, cuDoubleComplex *a, cuDoubleComplex *G, cuDoubleComplex *mt, double *vt, double m, double beta1, double beta2, double eps){

    for(int i=0; i<Nbank; i++) mt[i] = cuCadd( cdmu(mt[i],beta1) , cdmu( G[i], 1-beta1));
    for(int i=0; i<Nbank; i++) vt[i] = vt[i]*beta2 + cuCmul(G[i],cuConj(G[i])).x*( 1-beta2);


    for(int i=0; i<Nbank; i++){
        cuDoubleComplex hmt = cdmu( mt[i], 1./(1.-beta1));
        double hvt = vt[i]/(1-beta2);
        cuDoubleComplex da = cdmu( hmt, m/(sqrt(hvt)+eps));

        cuDoubleComplex at = cuCadd( a[i] , da);
        while( cabs(at) > 1.) {
            da = cdmu( da, 1./2);
            printf("\na[%d] = % -le % -le",i,carg(at),cabs(at));;
            at = cuCadd( a[i], da);
        }
        a[i] = at;
    }
}


double a1_opt( int nbank, std::complex<double> *ah, int Nbank, int *delayh, int npoint, std::complex<double> *wfh, int b0_size, std::complex<double> *b0 , double final_overlap_min){

  //printf( "Nbank = %d nbank = %d npoint = %d\n", Nbank, nbank, npoint);
    for(int i=0; i<Nbank; i++) delayh[i] = npoint-delayh[i];
    cudaStream_t stream;
    cudaStreamCreate( &stream);
    int grid = Nbank;
    int block = THX;
    int *delay = (int*)malloc_d(sizeof(int)*Nbank);
    CHECK( cudaMemcpyAsync( delay, delayh, sizeof(int)*Nbank, cudaMemcpyHostToDevice, stream));
    cuDoubleComplex *a = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*Nbank);
    CHECK( cudaMemcpyAsync( a, ah, sizeof(cuDoubleComplex)*Nbank, cudaMemcpyHostToDevice, stream));
    cuDoubleComplex *wf = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*npoint);
    CHECK( cudaMemcpyAsync( wf, wfh, sizeof(cuDoubleComplex)*npoint, cudaMemcpyHostToDevice, stream));

    cuDoubleComplex *b = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*Nbank);
    cuDoubleComplex *yt = (cuDoubleComplex*)malloc_d( sizeof(cuDoubleComplex)*Nbank);
    cuDoubleComplex *dyt = (cuDoubleComplex*)malloc_d( sizeof(cuDoubleComplex)*Nbank);
    cuDoubleComplex *yij = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*Nbank*Nbank);

    cusolverDnHandle_t handle = NULL;
    CHECKSOV( cusolverDnCreate( &handle));
    CHECKSOV( cusolverDnSetStream( handle, stream));
    int lwork = 2;
    int *info = (int*)malloc_u(sizeof(int)*1);
    cuDoubleComplex *inv_yij = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*Nbank*Nbank);

    CHECKSOV( cusolverDnZpotrf_bufferSize( handle, CUBLAS_FILL_MODE_UPPER, Nbank, inv_yij, Nbank, &lwork));
    cuDoubleComplex *workspace = (cuDoubleComplex*)malloc_u(sizeof(cuDoubleComplex)*lwork);

    cublasHandle_t handle_m = NULL;
    CHECKBLAS( cublasCreate( &handle_m));
    CHECKBLAS( cublasSetStream( handle_m, stream));

    cuDoubleComplex *yijb = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*Nbank);
    cuDoubleComplex *dyijda = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*Nbank*Nbank);
    cuDoubleComplex *dyijb = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*Nbank*Nbank);

    cuDoubleComplex *grad = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*Nbank);
    cuDoubleComplex *mt = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*Nbank);
    double *vt = (double*)malloc_d(sizeof(double)*Nbank);
    CHECK( cudaMemsetAsync( mt, 0, sizeof(cuDoubleComplex)*Nbank, stream));
    CHECK( cudaMemsetAsync( vt, 0, sizeof(double)*Nbank, stream));
    double m = 1.e-4, beta1 = 0.9, beta2 = 0.999, eps = 1.e-7;

    int Nrun = 10000;
    double overlap_max = 0;
    int i;
	for( i=0; i<Nrun; i++){

		// [[ determine b ]]
		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		calc_yt_dyt <<< grid, block, 2*block*sizeof(cuDoubleComplex), stream>>>( yt, dyt, a, delay, wf);

		int gridy = (Nbank*Nbank+block-1)/block;
		calculate_yij_cuda <<< gridy, block, 0, stream>>>( Nbank, a, delay, npoint, yij);

		// decomposition !!
		CHECK( cudaMemcpyAsync( inv_yij, yij, sizeof(cuDoubleComplex)*Nbank*Nbank, cudaMemcpyDeviceToDevice, stream));

		CHECKSOV( cusolverDnZpotrf( handle, CUBLAS_FILL_MODE_UPPER, Nbank, inv_yij,  Nbank, workspace, lwork, info));
		cudaStreamSynchronize( stream);
		if( *info != 0) printf("\nError with DnZpotrf count = %d info = %d\n\n", i, *info);
		if( *info != 0) exit( 1);

		CHECK( cudaMemcpyAsync( b, yt, sizeof(cuDoubleComplex)*Nbank, cudaMemcpyDeviceToDevice, stream));
		CHECKSOV( cusolverDnZpotrs( handle, CUBLAS_FILL_MODE_UPPER, Nbank, 1, inv_yij, Nbank, b, Nbank, info));
		cudaStreamSynchronize( stream);
		if( *info != 0) printf("\nError with DnZpotrs count = %d info = %d\n\n", i, *info);
		if( *info != 0) exit( 1);

		// here b[i] = conj(b) !!!

		// [[   normalize b0    ]]

		cuDoubleComplex bet = cmplx(0,0);
		cuDoubleComplex alpha = cmplx(1,0);

		// alpha & beta are constant multiple in this subroutine
		// yijb[j] = alpha * y[i][j] * b[i] + beta * yijb[j]
		CHECKBLAS( cublasZhemv( handle_m, CUBLAS_FILL_MODE_UPPER, Nbank, &alpha, yij, Nbank, b, 1, &bet, yijb, 1));

		cuDoubleComplex sum = cmplx(0,0);
		// sum += conj(b[i]) * yijb[i]
		CHECKBLAS( cublasZdotc( handle_m, Nbank, b, 1, yijb, 1, &sum));

		// get overlap
		cuDoubleComplex overlap = cmplx(1,0);
		CHECKBLAS( cublasZdotc( handle_m, Nbank, b, 1, yt, 1, &overlap));

		overlap = cuCdiv( overlap, cmplx(sqrt(sum.x),0));
		if( i==0) printf("\rorigin Overlap = % -13.12le % -13.12le", overlap.x, overlap.y);
		if( overlap.x > overlap_max && overlap.x-overlap_max < 1.e-8 ){
			overlap_max = overlap.x;
			break;
		}
		if( overlap_max < overlap.x) overlap_max = overlap.x;
		if( overlap_max > final_overlap_min && i > 2000) break;

		//.................................................................................................

		// [[   calculate differenctial of a    ]]
		// dyijda = dy[i][j]/da[i]
		calculate_dyij_dai<<< gridy, block, 0, stream>>>( Nbank, a, delay, dyijda);
		// the equation : conj(b) * conj(dyijdai) * b
		// in code eq.	: b[i] * conj(dyijda[i][j])/da[i] * conj(b[j])
		// 
		// yijb = b[i] * (dyijda[i][j]/da[i])^T
		CHECKBLAS( cublasZgemv( handle_m, CUBLAS_OP_N, Nbank, Nbank, &alpha, dyijda, Nbank, b, 1, &bet, dyijb, 1));

		calculate_gradient <<< gridy, block, 0, stream>>>( Nbank, dyijb, dyt, b, 1./sqrt(sum.x), grad);

		cudaStreamSynchronize( stream);
		update_a1_c <<< (Nbank+block-1)/block, block, 0, stream>>>( Nbank, a, grad, mt, vt, m, beta1, beta2, eps);
		cudaStreamSynchronize( stream);
		//update_a1( Nbank, a, grad, mt, vt, m, beta1, beta2, eps);

	}
	printf("\nniter = %d overlap = % -le\n", i, overlap_max);

  //for(int i=0; i<Nbank; i++) printf("%d % -g % -g\n", i, grad[i].x, grad[i].y);
    CHECK( cudaMemcpy( ah, a, sizeof(cuDoubleComplex)*Nbank, cudaMemcpyDeviceToHost));
    CHECK( cudaMemcpy( b0, b, sizeof(cuDoubleComplex)*Nbank, cudaMemcpyDeviceToHost));
    for(int i=0; i<Nbank; i++) b0[i] = conj(b0[i]);
    CHECK( cudaFree( a));
    CHECK( cudaFree( delay));
    CHECK( cudaFree( wf));
    CHECK( cudaFree( b));
    CHECK( cudaFree( yt));
    CHECK( cudaFree( dyt));
    CHECK( cudaFree( yij));
    CHECK( cudaFree( info));
    CHECK( cudaFree( inv_yij));
    CHECK( cudaFree( yijb));
    CHECK( cudaFree( workspace));
    CHECK( cudaFree( dyijda));
    CHECK( cudaFree( dyijb));
    CHECK( cudaFree( grad));
    CHECK( cudaFree( mt));
    CHECK( cudaFree( vt));
    CHECKSOV( cusolverDnDestroy( handle));
    CHECKBLAS( cublasDestroy( handle_m));
    cudaStreamDestroy( stream);
    return overlap_max;
}
