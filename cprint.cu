#include<iostream>
#include<complex>
#include<cuComplex.h>
#include<stdio.h>

__global__ void cprint_cdata( cuDoubleComplex *data, int N){
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id <N) printf("data[%d] = %g+%gj\n", id, cuCreal(data[id]), cuCimag(data[id]));
}
__global__ void cprint_data( double *data, int N){
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id <N) printf("data[%d] = %e\n", id, data[id]);
}
__global__ void cprint( int N){
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id <N) printf("this is thread number %d\n", id);
}
void cuda_print( int N){
    cprint <<< (N+32-1)/32, 32>>>( N);
}
void cuda_print_cdata( int N, std::complex<double> *data){

    cuDoubleComplex *data_d;
    cudaMalloc((void**)&data_d, sizeof(cuDoubleComplex)*N);
    cudaMemcpy( data_d, data, sizeof(cuDoubleComplex)*N, cudaMemcpyHostToDevice);
    cprint_cdata <<< (N+32-1)/32, 32>>>( data_d, N);
    cudaFree( data_d);
}
void cuda_print_data( int N, double *data){

    double *data_d;
    cudaMalloc((void**)&data_d, sizeof(double)*N);
    cudaMemcpy( data_d, data, sizeof(double)*N, cudaMemcpyHostToDevice);
    cprint_data <<< (N+32-1)/32, 32>>>( data_d, N);
    cudaFree( data_d);
}
