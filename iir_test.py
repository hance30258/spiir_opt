#!/usr/bin/env python

import numpy as np

def load_complex(f):
  t=np.loadtxt(f)
  return t[:,0]+1j*t[:,1]

a=load_complex('a1_h')

t=load_complex('template_h')
t/=np.sqrt(np.dot(np.conj(t),t))

d=np.loadtxt('delay_h')
d=np.array(d,dtype=np.int32)

import cuda_opt

b = np.zeros( np.size(d), dtype=complex)
overlap = cuda_opt.a1_opt( a, d, t, b, 1)

print("optimized overlap %f"% overlap)
