#include<stdio.h>
#include<iostream>
#include<complex>
#include<complex.h>
#include<cuComplex.h>
#include<math.h>
#include<time.h>
#include<cuda_profiler_api.h>
#include<cuda_runtime.h>
#include<cusolverDn.h>
#include<cublas_v2.h>
#include<omp.h>
#include"calloc.cu"
#include"optimizer_cuda.hu"



std::complex<double> *read_2D_txt(char *filename, int *n) {
    printf("txt file from    : %s \n",filename);

    FILE *ft_r = fopen(filename,"r");
    double rp, ip;
    int npoint=0;
    while(fscanf(ft_r,"%lf %lf",&rp,&ip )!=EOF) npoint++;
    fclose(ft_r);
    printf("npoint=%d\n",npoint);
    *n = npoint;

    FILE *fp_r = fopen(filename,"r");
    std::complex<double> *data = (std::complex<double>*)malloc_u(sizeof(std::complex<double>)*npoint);
    for(int i=0; i<npoint; i++){
        fscanf(fp_r,"%lf %lf",&rp,&ip );
        data[i] = { rp, ip};  
    }
    fclose(fp_r);

    printf("data: %lf %lf\n",data[0].real(), data[0].imag());
    printf("---------------------------------------\n");
    return data;
}
double *read_1D_txt(char *filename, int *n) {
    printf("txt file from    : %s \n",filename);

    FILE *ft_r = fopen(filename,"r");
    double rp;
    int npoint=0;
    while(fscanf(ft_r,"%lf",&rp )!=EOF) npoint++;
    fclose(ft_r);
    printf("npoint=%d\n",npoint);
    *n = npoint;

    FILE *fp_r = fopen(filename,"r");
    double *data = (double*)malloc_u(sizeof(double)*npoint);
    for(int i=0; i<npoint ;i++) {
        fscanf(fp_r,"%lf",&rp );
        data[i] = rp;
    }
    fclose(fp_r);
    printf("---------------------------------------\n");
    return data;
}


int main( int argc, char **argv){

    int Nbank, npoint;

    std::complex<double> *a = read_2D_txt( argv[1], &Nbank);
    std::complex<double> *wf = read_2D_txt( argv[3], &npoint);
    std::complex<double> *wfd = (std::complex<double>*)malloc_d(sizeof(std::complex<double>)*npoint);
    cudaMemcpy( wfd, wf, sizeof(std::complex<double>)*npoint, cudaMemcpyHostToDevice);

    double *delayd = read_1D_txt( argv[2], &Nbank);
    int *delay = (int*)malloc_u(sizeof(int)*Nbank);
    // delay == point number of template
  //for(int i=0; i<Nbank; i++) delay[i] = npoint-delayd[i];
    for(int i=0; i<Nbank; i++) delay[i] = delayd[i];

    // normalize
    double sum;
    for(int i=0; i<npoint; i++) sum += real(wf[i]*conj(wf[i]));
    sum = 1./sqrt(sum);
    for(int i=0; i<npoint; i++) wf[i] = wf[i]* sum;

    std::complex<double> a_origin[Nbank];
    for(int i=0; i<Nbank; i++) a_origin[i] = a[i];

  //FILE *ao = fopen("a1_origin.txt","w");
  //for(int i=0; i<Nbank; i++) fprintf(ao,"% -15.13le % -15.13le\n", i,a_origin[i].x,a_origin[i].y);
  //fclose(ao);



  //return 0;

    int num = 1;
    std::complex<double> *acpu[num];
    for(int i=0;i<num;i++) acpu[i] = (std::complex<double>*)malloc_u(sizeof(std::complex<double>)*Nbank);
    for(int i=0;i<num;i++) cudaMemcpy( acpu[i], a, sizeof(std::complex<double>)*Nbank, cudaMemcpyDeviceToDevice);

    clock_t start, end;
    start = clock();
        omp_set_num_threads(num);
        int id_cpu;
#pragma omp parallel private( id_cpu)
        {
            id_cpu = omp_get_thread_num();
            printf("id_cpu = %d\n",id_cpu);
            cudaSetDevice( 0);

            a1_opt( Nbank, acpu[id_cpu], Nbank, delay, npoint, wf);
        }

    end = clock();
    printf("cpu time = %e\n",(double)(end-start)/num/CLOCKS_PER_SEC);
  //FILE *ar = fopen("a1_out.txt","w");
  //for(int i=0; i<Nbank; i++) fprintf(ar,"% -15.13le % -15.13le\n", i,a[i].x,a[i].y);
  //fclose(ar);

    return 0;
}
