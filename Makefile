CPP = g++ -fPIC -O2
NVCC = nvcc -Xcompiler -fopenmp -Xcompiler -fPIC -O3
SWIG = swig -c++ -python

# change the direction if needed
CUDA_LD_PATH = /pkg/cuda/9.1.85/lib64
NUMPY_INCLUDE_PATH = $(HOME)/.local/lib/python2.7/site-packages/numpy/core/include

CUDA_FLAGS = -lcudart -lcuda -lcusolver -lcublas -L$(CUDA_LD_PATH)
PYTHON_FLAGS = -I/usr/include/python2.7 -I$(NUMPY_INCLUDE_PATH)
CFLAGS = $(CUDA_FLAGS)

pymodule_name = cuda_opt
object = calloc.o optimizer_cuda.o cprint.o $(pymodule_name)_wrap.o
shared_object = _$(pymodule_name).so
swig_wrap = $(swigfile)

$(shared_object): $(object)
	g++ -shared $^ -o $@ $(CFLAGS)

%.o: %.cu
	$(NVCC) -c -o $@ $^
%.o: %.cxx
	$(CPP) -c -o $@ $^ $(PYTHON_FLAGS)
%_wrap.cxx: %.i
	$(SWIG) $^

all: $(shared_object)

clean:
	rm *.o *.so $(pymodule_name).py $(pymodule_name).pyc
