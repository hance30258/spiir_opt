import cuda_opt
import numpy as np

cuda_opt.cuda_print( 3)
a = np.array( [1,2,3])
cuda_opt.cuda_print_data( a)
ac = np.array( [1+1j,2+2j,3+3j])
cuda_opt.cuda_print_cdata(ac)
