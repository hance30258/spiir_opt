
%module cuda_opt
%include <std_complex.i>

%{
#define SWIG_FILE_WITH_INIT
#include "cprint.hu"
#include "optimizer_cuda.hu"
%}

%include "numpy.i"
%init %{
import_array();
%}

%apply(int DIM1, double* IN_ARRAY1) {(int N, double *data)}
%apply(int DIM1, std::complex<double>* IN_ARRAY1) {(int N, std::complex<double> *data)}
%include "cprint.hu"
%apply(int DIM1, std::complex<double>* INPLACE_ARRAY1) {(int nbank, std::complex<double> *ah)}
%apply(int DIM1, int* IN_ARRAY1) {(int Nbank, int *delayh)}
%apply(int DIM1, std::complex<double>* IN_ARRAY1) {(int npoint, std::complex<double>* wfh)}
%apply(int DIM1, std::complex<double>* INPLACE_ARRAY1) {(int b0_size, std::complex<double> *b0)}
%include "optimizer_cuda.hu"

